* website: <https://arrizza.com/arduino-serial-output.html>
* installation: see <https://arrizza.com/setup-common.html>

## Summary

Prints text to the Arduino Serial Port.

This is a simple Arduino App that blinks the LED and also outputs some data on the serial port. To see
that output you need to set up a terminal emulator like Putty or screen and configure it correctly (115200, 8n1).

## Configure cmake

See [Arduino Setup](https://arrizza.com/arduino-setup) for details on how to set up CMakeLists.txt for your arduino and
port.

## Setup

This also sets up the serial port at the 115200 baud rate. A baud is 1 bit per second, so 115200 baud rate is 115,200
bits per second. There are other bits that may get sent for each byte (a start bit, a parity bit and a stop bit).

It also sends a message "setup" to the serial port.

```cpp
Serial.begin(115200);
Serial.println("setup");
```

Serial.begin() is a function that sets up the serial port to the given baud rate, in this case 115,200 bps.

The Serial.println() is a function that prints the parameter given to it, in this case the word "
setup" and follows it with a newline.

A newline is a carriage return or a linefeed or both, depending on which Operating System you are on. This is equivalent
to pressing the Enter key on the keyboard - it puts a new line in the output. It is sometimes abbreviated as "nl" and
sometimes it is referred to as "\n"
(this is because in many languages "\n" represents a newline sequence).

A carriage return, sometimes abbreviated as "cr", is a single character with hex value 0x0D.

A linefeed, sometimes abbreviated as "lf", is a single character with hex value 0x0A. This is used for most Linux or
Unix based systems and for the Arduino.

On Windows, a newline is a two character combination 0x0D 0x0A (carriage return/line feed).

## Main Loop

The main loop is similar to "Main Loop" in [Arduino LED Blink](https://arrizza.com/arduino-led-blink). It also prints
a "1' and "0" to the serial port every second.

```cpp
Serial.print("1");

Serial.print("0");
```

The Serial.print() is a function that is identical to the Serial.println() except it does NOT print the newline
character. Because of the newline is missing, the "1" and "0" will be side-by-side on the terminal output.

```bash
   setup
   101010101010
```

It will continue writing the "10" sequence until you unplug the USB cable.

## Notes

* You must have the terminal emulator (e.g. Putty) configured correctly.
    * for CLion, see "Setup Serial Monitor in CLion" in [Arduino Setup](https://arrizza.com/arduino-setup)
    * for others, see "Setup putty or screen" in [Arduino Setup](https://arrizza.com/arduino-setup)
    * Don't forget. To upload a new version to the Arduino, you have to disconnect the terminal emulator. And that
      re-plugging in the terminal emulator will reboot your Arduino.

* You should see a "1" when the LED is blinking quickly, and you should see a "0" when the LED is off.

* Try changing the text inside the Serial.println() or Serial.print() statements. You should see whatever you typed in
  there, showing up on your terminal emulator.

* Try changing the Serial.print() to Serial.println(). You should see a newline after each 1 and 0, something like:

```bash
 0
 1
 0
 1
 0
# ... etc ...
```
